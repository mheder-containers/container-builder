#!/bin/sh
set -eux

: "${container_name:="builder"}"
: "${target_image_base:="${CI_REGISTRY}/${CI_PROJECT_PATH}/${container_name}"}"

build_container ()
{
  ansible-bender build container/ansible/builder.yml ${BASE_IMAGE} $1
}

if [ -z "${CI_COMMIT_TAG+x}" ] ; then
  build_container ${target_image_base}:${CI_COMMIT_REF_NAME}
  buildah push "${target_image_base}:${CI_COMMIT_REF_NAME}"
else
  major_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1)
  minor_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1,2)
  patch_tag=$(echo "${CI_COMMIT_TAG}" | grep -E '^v[0-9]+\.[0-9]+(\.[0-9]+)?(-.+)?$')
  if [ -z "${patch_tag}" ] ; then
    printf 'Error: the latest '"${CI_COMMIT_TAG}"' tag is not starts with v[0-9].[0-9]%s\n'
    exit 1
  fi
  build_container ${target_image_base}:tagged
  for tag in "${major_tag}" "${minor_tag}" "${patch_tag}" ; do
    buildah tag "${target_image_base}:tagged" "${target_image_base}:${tag}"
    buildah push "${target_image_base}:${tag}"
  done
fi
