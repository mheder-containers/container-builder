# Container Builder
This project and pipeline create an image what can be used to create other images.
Docker in docker used for run, privileged docker container required for it.
Key applications:
- ansible
- ansible-bender
- buildah
- podman

I used Fedora docker image as base because it contains the recent application versions. In Ansible only one role is included: **builder** which trigger the applications installation and change some settings what are required for buildah/podman.
Ansible-bender trigger the ansible run, call buildah and podman for container operations and store the created container in GitLab registry.
